use clap::Parser;
use lazy_static::lazy_static;
use main_error::MainError;
use regex::Regex;
use serde_json::{self, Map, Value};
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::process;

use subnautica_tw::*;

const TW_BEST: &str = include_str!("../../resources/Chinese (60026Test).json");
const TW_GOOD: &str = include_str!("../../resources/Subnautica繁體中文化 ChineseT.json");
const EN_FILE: &str = "/home/rypervenche/.local/share/Steam/steamapps/common/Subnautica/Subnautica_Data/StreamingAssets/SNUnmanagedData/LanguageFiles/English.json";
const CN_FILE: &str = "/home/rypervenche/.local/share/Steam/steamapps/common/Subnautica/Subnautica_Data/StreamingAssets/SNUnmanagedData/LanguageFiles/Chinese (Simplified).json";
const CN_FILE_BAK: &str = "/home/rypervenche/.local/share/Steam/steamapps/common/Subnautica/Subnautica_Data/StreamingAssets/SNUnmanagedData/LanguageFiles/Chinese (Simplified).json.bak";

lazy_static! {
    static ref TW_BEST_JSON: Map<String, Value> = to_json(TW_BEST).unwrap();
    static ref TW_GOOD_JSON: Map<String, Value> = to_json(TW_GOOD).unwrap();
    static ref CN_JSON: Map<String, Value> = if Path::new(CN_FILE_BAK).is_file() {
        to_json(CN_FILE_BAK).unwrap()
    } else {
        to_json(CN_FILE).unwrap()
    };
    static ref EN_FILE_JSON: Map<String, Value> = to_json(EN_FILE).unwrap();
}

#[derive(Parser)]
#[clap(version, about = "Subnautica zh_TW translation merger")]
pub struct Opts {
    /// Append "(CN_FILE)" to end of CN_FILE strings
    #[clap(short, long)]
    pub testing: bool,

    /// Output as CN_FILE file to get Chinese logo
    #[clap(short, long)]
    pub cn_overwrite: bool,

    /// Custom output file
    #[clap(short, long, value_name("file"))]
    pub output_file: Option<String>,

    /// Reset JSON files to original state
    #[clap(short, long)]
    pub reset: bool,
}

fn search_and_replace(value: Value) -> Result<Value, Box<dyn Error>> {
    lazy_static! {
        // Punctuation
        static ref RE_ELIPSIS: Regex = Regex::new(r"\.\.\.?|。。。?").unwrap();
        static ref RE_PERIOD: Regex = Regex::new(r"。?\.").unwrap();
        static ref RE_UNNEEDED_SPACES: Regex = Regex::new(r" *([…，！。]+) *").unwrap();
    }

    let mut value_str = value
        .as_str()
        .unwrap_or_else(|| {
            println!("Failed to unwrap: {value}");
            process::exit(1);
        })
        .to_string();

    value_str = replace(",", "，", value_str)?;
    value_str = replace("!", "！", value_str)?;

    value_str = re_replace(&RE_ELIPSIS, "…", value_str)?;
    value_str = re_replace(&RE_PERIOD, "。", value_str)?;
    value_str = re_replace(&RE_UNNEEDED_SPACES, "$1", value_str)?;

    value_str = replace("海鵝", "海蛾", value_str)?;
    value_str = replace("納米", "奈米", value_str)?;

    Ok(Value::String(value_str))
}

fn append_cn(value: Value) -> Result<Value, Box<dyn Error>> {
    let mut value_str = value
        .as_str()
        .unwrap_or_else(|| {
            println!("Failed to unwrap: {value}");
            process::exit(1);
        })
        .to_string();

    value_str += "(CN_FILE)";

    Ok(Value::String(value_str))
}

fn find_best_value(testing: bool) -> Result<Map<String, Value>, Box<dyn Error>> {
    let mut best_json = Map::new();

    'outer: for (en_key, en_value) in EN_FILE_JSON.iter() {
        if let Some(tw_best_value) = TW_BEST_JSON.get(en_key) {
            if *tw_best_value != Value::Null && is_good_hanzi(tw_best_value) {
                let tw_best_value = search_and_replace(tw_best_value.to_owned())?;
                best_json.insert(en_key.to_string(), tw_best_value.to_owned());
                continue 'outer;
            }
        }

        if let Some(tw_good_value) = TW_GOOD_JSON.get(en_key) {
            if *tw_good_value != Value::Null && is_good_hanzi(tw_good_value) {
                let tw_good_value = search_and_replace(tw_good_value.to_owned())?;
                best_json.insert(en_key.to_string(), tw_good_value.to_owned());
                continue 'outer;
            }
        }

        if let Some(cn_value) = CN_JSON.get(en_key) {
            if *cn_value != Value::Null && is_good_hanzi(cn_value) {
                let trad_value = simp_to_trad(cn_value);
                let mut trad_value = search_and_replace(trad_value.to_owned())?;

                if testing {
                    trad_value = append_cn(trad_value)?;
                }

                best_json.insert(en_key.to_string(), trad_value);
                continue 'outer;
            }
        }

        best_json.insert(en_key.to_string(), en_value.to_owned());
    }

    Ok(best_json)
}

fn manual_updates(json: &mut Map<String, Value>) -> Result<(), Box<dyn Error>> {
    update_value("Yes", "是", json)?;
    update_value("No", "否", json)?;
    add_value("LanguageChinese (Traditional)", "正體中文", json)?;
    update_value("SubnauticaStoreSecondary", "T恤，海報還有更多！", json)?;
    update_value("EarlyAccessLabel", "深海迷航", json)?;
    update_value("EarlyAccessWatermarkFormat", "{0:yyyy年M月} {1}", json)?;
    update_value("DateFormat", "{0:yyyy年MM月d日 H:mm}", json)?;
    update_value("CompassDirectionE", "東", json)?;
    update_value("CompassDirectionN", "北", json)?;
    update_value("CompassDirectionNE", "東北", json)?;
    update_value("CompassDirectionNW", "西北", json)?;
    update_value("CompassDirectionS", "南", json)?;
    update_value("CompassDirectionSE", "東南", json)?;
    update_value("CompassDirectionSW", "西南", json)?;
    update_value("CompassDirectionW", "西", json)?;
    update_value("FirstAidKit", "急救箱", json)?;
    update_value("AluminumOxide", "紅寶石", json)?;
    update_value("Silicone", "矽膠", json)?;
    update_value("DrillableAluminumOxide", "紅寶石", json)?;
    update_value("SignalDistanceFormat", "{0}公尺", json)?;
    update_value("SignalDistanceFormatWithDescription", "{0}\n{1}公尺", json)?;

    Ok(())
}

fn main() -> Result<(), MainError> {
    let opts: Opts = Opts::parse();

    if opts.reset {
        reset_files()?;
        process::exit(0);
    }

    let output_file = if opts.output_file.is_some() {
        opts.output_file.unwrap()
    } else {
        manage_output_files(opts.cn_overwrite)?
    };

    let mut best_json = find_best_value(opts.testing)?;

    manual_updates(&mut best_json)?;

    if opts.cn_overwrite {
        update_value("LanguageChinese (Simplified)", "正體中文", &mut best_json)?;
    }

    let pretty_json = serde_json::to_string_pretty(&best_json)?;

    let mut buffer = File::create(output_file).expect("Error creating file");
    buffer
        .write_all(pretty_json.as_bytes())
        .expect("Unable to write to file");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_opencc() {
        let simp = Value::String("忧郁".to_string());
        let trad = simp_to_trad(&simp);
        assert_eq!(Value::String("憂鬱".to_string()), trad);
    }

    #[test]
    fn check_traditional() {
        let simp = Value::String("为什么".to_string());
        let trad = simp_to_trad(&simp);
        assert_eq!(Value::String("為什麼".to_string()), trad);
    }

    #[test]
    fn check_regex() {
        let good1 = Value::String("紅寶石".to_string());
        let good2 = Value::String("20%".to_string());
        let good3 = Value::String("準備: 20%".to_string());
        let good4 = Value::String("中文: English".to_string());
        let good5 = Value::String("English: 中文".to_string());
        let bad1 = Value::String("Ruby".to_string());
        let bad2 = Value::String("Thing: 200%".to_string());

        assert!(is_good_hanzi(&good1));
        assert!(is_good_hanzi(&good2));
        assert!(is_good_hanzi(&good3));
        assert!(is_good_hanzi(&good4));
        assert!(is_good_hanzi(&good5));
        assert!(!is_good_hanzi(&bad1));
        assert!(!is_good_hanzi(&bad2));
    }

    #[test]
    fn convert_punctuation() {
        let good1 = Value::String("Nothing special".to_string());
        let bad1 = Value::String("Something special.".to_string());
        let bad2 = Value::String("One, two.".to_string());
        let bad3 = Value::String("Hmmm...".to_string());
        let bad4 = Value::String("Hmmm. This . That.".to_string());
        let bad5 = Value::String("這些有機部件DNA顯示含有數種生物, 大部分來自 offworld. 利用先進的技術讓它們在不同的狀態下組織起來. 這條生產線的設置顯示出裂空者是製造出來的, 經由外星人們所設計建造出來的佈署維護設施.".to_string());
        let bad6 = Value::String(
            "我告訴過她，別人會來的。.\n\n我被拋出了基地，這東西就轉身撲向我,上我的東西。"
                .to_string(),
        );

        let fixed_good1 = Value::String("Nothing special".to_string());
        let fixed_bad1 = Value::String("Something special。".to_string());
        let fixed_bad2 = Value::String("One，two。".to_string());
        let fixed_bad3 = Value::String("Hmmm…".to_string());
        let fixed_bad4 = Value::String("Hmmm。This。That。".to_string());
        let fixed_bad5 = Value::String("這些有機部件DNA顯示含有數種生物，大部分來自 offworld。利用先進的技術讓它們在不同的狀態下組織起來。這條生產線的設置顯示出裂空者是製造出來的，經由外星人們所設計建造出來的佈署維護設施。".to_string());
        let fixed_bad6 = Value::String(
            "我告訴過她，別人會來的。\n\n我被拋出了基地，這東西就轉身撲向我，上我的東西。"
                .to_string(),
        );

        assert_eq!(search_and_replace(good1).unwrap(), fixed_good1);
        assert_eq!(search_and_replace(bad1).unwrap(), fixed_bad1);
        assert_eq!(search_and_replace(bad2).unwrap(), fixed_bad2);
        assert_eq!(search_and_replace(bad3).unwrap(), fixed_bad3);
        assert_eq!(search_and_replace(bad4).unwrap(), fixed_bad4);
        assert_eq!(search_and_replace(bad5).unwrap(), fixed_bad5);
        assert_eq!(search_and_replace(bad6).unwrap(), fixed_bad6);
    }

    #[test]
    fn cn_to_tw() {
        let bad1 = Value::String("海鵝號".to_string());
        let bad2 = Value::String("外星科技納米機器人".to_string());

        let fixed_bad1 = Value::String("海蛾號".to_string());
        let fixed_bad2 = Value::String("外星科技奈米機器人".to_string());

        assert_eq!(search_and_replace(bad1).unwrap(), fixed_bad1);
        assert_eq!(search_and_replace(bad2).unwrap(), fixed_bad2);
    }

    #[test]
    fn update_json_values() {
        let mut base1: Map<String, Value> = Map::new();
        base1.insert("Animal".to_string(), Value::String("Dog".to_string()));
        let mut updated: Map<String, Value> = Map::new();
        updated.insert("Animal".to_string(), Value::String("Cat".to_string()));

        let mut base2: Map<String, Value> = Map::new();
        base2.insert("Animal".to_string(), Value::String("Dog".to_string()));
        let mut added1: Map<String, Value> = Map::new();
        added1.insert("Animal".to_string(), Value::String("Dog".to_string()));
        added1.insert("Animal2".to_string(), Value::String("Cat".to_string()));

        let mut base3: Map<String, Value> = Map::new();
        base2.insert("Animal".to_string(), Value::String("Dog".to_string()));
        let mut added2: Map<String, Value> = Map::new();
        added2.insert("Animal".to_string(), Value::String("Dog".to_string()));
        added2.insert("Animal2".to_string(), Value::String("Cat".to_string()));

        update_value("Animal", "Cat", &mut base1).unwrap();
        add_value("Animal2", "Cat", &mut base2).unwrap();
        add_value("Animal", "Cat", &mut base3).unwrap();

        assert_eq!(updated, base1);
        assert_eq!(added1, base2);
        assert_ne!(added2, base3);
    }
}
