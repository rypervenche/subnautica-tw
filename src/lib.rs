use lazy_static::lazy_static;
use opencc_rust::*;
use regex::Regex;
use serde_json::{self, Map, Value};
use std::error::Error;
use std::fs;
use std::path::Path;
use std::process;
use strip_bom::*;

const TW_FILE: &str = "/home/rypervenche/.local/share/Steam/steamapps/common/Subnautica/Subnautica_Data/StreamingAssets/SNUnmanagedData/LanguageFiles/Chinese (Traditional).json";
const CN_FILE: &str = "/home/rypervenche/.local/share/Steam/steamapps/common/Subnautica/Subnautica_Data/StreamingAssets/SNUnmanagedData/LanguageFiles/Chinese (Simplified).json";
const CN_FILE_BAK: &str = "/home/rypervenche/.local/share/Steam/steamapps/common/Subnautica/Subnautica_Data/StreamingAssets/SNUnmanagedData/LanguageFiles/Chinese (Simplified).json.bak";

pub fn to_json(file: &str) -> Result<Map<String, Value>, Box<dyn Error>> {
    let data = if Path::new(file).is_file() {
        fs::read_to_string(file).expect("Could not read file")
    } else {
        file.to_string()
    };
    let data = &data.strip_bom();
    let hjson: Value = deser_hjson::from_str(data).expect("Failed to parse HJSON");
    let json_str: String = serde_json::to_string(&hjson)?;
    let json: Map<String, Value> = serde_json::from_str(&json_str)?;

    Ok(json)
}

pub fn simp_to_trad(simp: &Value) -> Value {
    let simp_str = simp.as_str().unwrap();
    let cc = OpenCC::new(DefaultConfig::S2TWP).unwrap();
    let trad_str = cc.convert(simp_str);

    Value::String(trad_str)
}

pub fn is_good_hanzi(value: &Value) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(
            r"(?x)
                \p{Han}+                                # Hanzi
                |
                ^
                (^(^[\p{Nd}\pS\pP\p{Zs}]+|^\P{Latin}+)) # Symbols with no Latin chars
                $
            "
        )
        .unwrap();
    }

    let value_str = value.as_str().unwrap_or_else(|| {
        println!("Failed to unwrap: {value}");
        process::exit(1);
    });

    RE.is_match(value_str)
}

pub fn replace(from: &str, to: &str, orig: String) -> Result<String, Box<dyn Error>> {
    if orig.contains(from) {
        let string = orig.replace(from, to);
        Ok(string)
    } else {
        Ok(orig)
    }
}

pub fn re_replace(regex: &Regex, to: &str, orig: String) -> Result<String, Box<dyn Error>> {
    if regex.is_match(&orig) {
        let string = regex.replace_all(&orig, to).to_string();
        Ok(string)
    } else {
        Ok(orig)
    }
}

pub fn add_value(
    key: &str,
    value: &str,
    json: &mut Map<String, Value>,
) -> Result<(), Box<dyn Error>> {
    if json
        .insert(key.to_string(), Value::String(value.to_string()))
        .is_some()
    {
        panic!("Expected None");
    };

    Ok(())
}

pub fn update_value(
    key: &str,
    value: &str,
    json: &mut Map<String, Value>,
) -> Result<(), Box<dyn Error>> {
    if json
        .insert(key.to_string(), Value::String(value.to_string()))
        .is_none()
    {
        panic!("Expected Some");
    };

    Ok(())
}

pub fn manage_output_files(cn_overwrite: bool) -> Result<String, Box<dyn Error>> {
    if cn_overwrite {
        if Path::new(TW_FILE).is_file() {
            fs::remove_file(TW_FILE)?;
        }
        if !Path::new(CN_FILE_BAK).is_file() {
            fs::copy(CN_FILE, CN_FILE_BAK)?;
        }
        Ok(CN_FILE.to_owned())
    } else {
        if Path::new(CN_FILE_BAK).is_file() {
            fs::rename(CN_FILE_BAK, CN_FILE)?;
        }
        Ok(TW_FILE.to_owned())
    }
}

pub fn reset_files() -> Result<(), Box<dyn Error>> {
    if Path::new(TW_FILE).is_file() {
        fs::remove_file(TW_FILE)?;
    }
    if Path::new(CN_FILE_BAK).is_file() {
        fs::rename(CN_FILE_BAK, CN_FILE)?;
    }
    Ok(())
}
